use std::{rc::Rc};
use std::{borrow::Borrow, cell::RefCell};
use wasm_bindgen::prelude::*;
use wasm_bindgen::{JsValue, JsCast};
use crate::{RenderResult, RenderContext, Score};
// use crate::util::{get_window, get_document};
// use web_sys::{Document, HtmlElement, HtmlTextAreaElement, HtmlCanvasElement, CanvasRenderingContext2d};

//渲染系统，含输入语法解析后的对象渲染过程
pub struct System{
  pub context: Rc<RenderContext>,
  pub score: Rc<RefCell<Score>>
}

impl System {
  pub fn new(
    context: Rc<RenderContext>,
  ) -> System {
    System {
      context,
      score: Rc::new(RefCell::new(Score::create_empty_score())),
    }
  }

  pub fn draw(&mut self) -> RenderResult<()>{
    let _context = &self.context;
    let _score = self.score.borrow_mut();
    // score.draw(context);
    Ok(())
  }
}
