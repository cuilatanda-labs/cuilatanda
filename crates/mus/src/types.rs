use wasm_bindgen::{JsValue};
use std::cell::RefCell;
use std::rc::Rc;
use serde::{Deserialize, Serialize};
use web_sys::CanvasRenderingContext2d;
pub type RenderResult<T> = std::result::Result<T, JsValue>;

#[derive(Clone, Debug, PartialEq, Serialize)]
pub struct Point {
  pub x: f64,
  pub y: f64
}

impl Point {
  pub fn new(x: f64, y: f64) -> Point{
    Point{x, y}
  }
}

//扩展
pub struct RenderContext {
  pub context: Rc<CanvasRenderingContext2d>,
  pub width: f64,
  pub height: f64,
  pub pixel_ratio: f64,
}
