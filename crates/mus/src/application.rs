use crate::{System, RenderResult, RenderContext};
use std::cell::RefCell;
use std::rc::Rc;
use futures::*;
use wasm_bindgen::prelude::*;
use wasm_bindgen::{JsCast};
use web_sys::{Document, HtmlCanvasElement, CanvasRenderingContext2d, MouseEvent};

use crate::util::{get_window, get_document};

//画布程序
pub struct Application {
  //简谱渲染及解析系统
  canvas: HtmlCanvasElement,
  system: Rc<RefCell<System>>
}

impl Application {

  pub fn create(canvas_id: &str, width: f64, height: f64) -> RenderResult<Application> {
    let document = get_document().unwrap();

    let (canvas, context) = create_canvas(&document, width, height, canvas_id)?;
    let canvas_width = width * context.pixel_ratio;
    let canvas_height = height * context.pixel_ratio;
    canvas.set_width( canvas_width as u32);
    canvas.set_height( canvas_height as u32);

    let application = Application {
      canvas,
      system:  Rc::new(RefCell::new(System::new(context)))
    };
    Ok(application)
  }

  pub fn start(&self) -> RenderResult<()> {
    let system = self.system.clone();
    {
      let muse_down_handler = Closure::wrap(Box::new(move |event: MouseEvent| {
        log::info!("canvas event registry. {},{}", event.offset_x(), event.offset_y());
        system.borrow_mut().draw().expect("something's gone wrong.");
      }) as Box<dyn FnMut(_)>);
      self.canvas.add_event_listener_with_callback("mousedown", muse_down_handler.as_ref().unchecked_ref())?;
      muse_down_handler.forget();
    }
    {
      let context_menu_handler = Closure::wrap(Box::new(move |event: MouseEvent|{
        event.prevent_default();
        log::info!("canvas context menu");
        //TODO 增加右键菜单项.
      }) as Box<dyn FnMut(_)>);
      self.canvas.add_event_listener_with_callback("contextmenu", context_menu_handler.as_ref().unchecked_ref())?;
      context_menu_handler.forget();
    }

    Ok(())
  }

}


pub fn create_canvas(
  document: &Document,
  width: f64,
  height: f64,
  canvas_id: &str
) -> RenderResult<(HtmlCanvasElement, Rc<RenderContext>)> {
  let pixel_ratio = get_window().unwrap().device_pixel_ratio() as f64;

  let canvas = document
    .get_element_by_id(canvas_id)
    .unwrap()
    .dyn_into::<HtmlCanvasElement>()?;


  log::info!("device scale is {}", pixel_ratio);
  let context = Rc::new(
    canvas
      .get_context("2d")?
      .unwrap()
      .dyn_into::<CanvasRenderingContext2d>()?,
  );

  //貌似这里设置了未生效.
  context.scale(pixel_ratio, pixel_ratio)?;

  let render_context = Rc::new(RenderContext{
    context,
    width,
    height,
    pixel_ratio
  });


  Ok((canvas, render_context))
}
