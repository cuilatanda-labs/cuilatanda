//音符时值类型
use serde::{Serialize};

#[derive(Clone, Debug, Serialize)]
pub enum DurationType{
  VWHOLE,
  VQUARTER,
  VHALF,
  VEIGHTH,
  V16TH,
  V32TH,
  V32ND,
  V64TH,
  V128TH,
  V256TH,
  V512TH,
  V1024TH,
  VBREVE,
  VLONG,
  VZERO
}
