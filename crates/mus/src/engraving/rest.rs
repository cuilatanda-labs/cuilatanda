use serde::{Serialize};
use crate::engraving::DurationType;

// 休止符
#[derive(Clone, Debug, Serialize)]
pub struct Rest {
  pub duration_type: DurationType
}

impl Rest{
  pub fn create_default() -> Rest {
    Rest{
      duration_type: DurationType::VWHOLE
    }
  }
}
