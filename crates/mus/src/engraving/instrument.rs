use serde::{Serialize};
use crate::engraving::Tune;

#[derive(Clone, Debug, Serialize)]
pub enum InstrumentType {
  Zheng,
  Pipa
}

//　乐器信息
#[derive(Clone, Debug, Serialize)]
pub struct Instrument {
  pub instrument_type: InstrumentType,
  pub tune: Tune
}

impl Instrument{
  pub fn create_default() -> Instrument{
    Instrument{
      instrument_type: InstrumentType::Zheng,
      tune: Tune::create_default()
    }
  }
}
