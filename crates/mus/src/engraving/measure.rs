use serde::{Serialize};
use crate::engraving::Voice;
// 小节
#[derive(Clone, Debug, Serialize)]
pub struct Measure{
  pub voice: Voice
}

impl Measure {

  pub fn create_default() -> Measure {
    let voice = Voice::create_default();
    Measure {
      voice
    }
  }
}
