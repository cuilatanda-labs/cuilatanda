use crate::engraving::Instrument;
use serde::{Serialize};

// 乐器轨信息，含名称，调号等
#[derive(Clone, Debug, Serialize)]
pub struct Track {
  pub id: i32,
  pub name: String,
  pub instrument: Instrument
}

impl Track {
  pub fn create_default() -> Track{
    Track{
      id: 1,
      name: "古筝".to_string(),
      instrument: Instrument::create_default()
    }
  }
}
