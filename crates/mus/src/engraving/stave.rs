use serde::{Serialize};
use crate::engraving::Measure;

//某一音轨谱面信息, 含对应的乐器信息，小节等
#[derive(Clone, Debug, Serialize)]
pub struct Stave {
  pub track_id: i32,
  pub measures: Vec<Measure>
}

impl Stave {
  pub fn create_default() -> Stave {
    let track_id = 1;
    let measures = vec![Measure::create_default(), Measure::create_default(), Measure::create_default(), Measure::create_default()];
    Stave{
      track_id,
      measures
    }
  }
}
