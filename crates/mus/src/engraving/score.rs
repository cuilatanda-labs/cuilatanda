use serde::{Serialize};
use crate::RenderResult;
use crate::engraving::{Stave, Track};

#[derive(Clone, Debug, Serialize)]
pub struct Score{
  pub tracks: Vec<Track>,
  pub staves: Vec<Stave>
}

impl Score{

  // create a score object of default.
  pub fn create_empty_score() -> Score {
    let staves = vec![Stave::create_default()];
    let tracks = vec![Track::create_default()];
    Score{
      staves,
      tracks
    }
  }

  pub fn draw() -> RenderResult<()> {

    Ok(())
  }
}

#[test]
fn test_score_object() {
  let score = Score::create_empty_score();
  let json = serde_json::to_string(&score).unwrap();
  println!("{}", json);

}
