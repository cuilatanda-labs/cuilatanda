use serde::{Serialize};

// 音符，含midi音高, 八度
// midi pitch: midi通信协议的字节，C3＝60, A3=69, C3~C4: 60(C3) 61(C3#) 62(D3) 63(D3#) 64(E3)  65(F3)  66(F3#)  67(G3)  68(G3#)  69(A3)  70(A3#) 71(B3) 72(C4)
#[derive(Clone, Debug, Serialize)]
pub struct Note{
  pub pitch: i64,
  pub octave: i64
}
