use crate::RenderResult;
use web_sys::{Window, Document, HtmlElement, Element};


pub fn get_window() -> RenderResult<Window> {
  Ok(web_sys::window().unwrap())
}

pub fn get_document() -> RenderResult<Document> {
  let window = get_window().unwrap();
  Ok(window.document().unwrap())
}
