use std::cell::RefCell;
use std::rc::Rc;

use crate::RenderContext;
use crate::engraving::Score;

// render score in canvas.
pub struct Renderer {
  pub context: RenderContext,
  pub score: Rc<RefCell<Score>>
}


impl Renderer {

  //开始渲染谱面
  pub fn draw_score(&mut self) {

  }

  //编辑窗口发生变化
  pub fn resize(&mut self){

  }

}
