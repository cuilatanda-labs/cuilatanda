
mod render;
mod editor;
mod state;
mod ui_control;
mod glyph;

use crate::types::{RenderContext, RenderResult, Point};

pub trait Render{
  fn draw(&self, ctx: &RenderContext) -> RenderResult<Point>;
}
