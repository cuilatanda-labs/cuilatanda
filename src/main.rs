use vizia::prelude::*;
use std::{error::Error, time::Duration};
use std::sync::{
  atomic::{AtomicBool, Ordering},
  Arc,
};

mod app_data;
mod ui;
mod engraving;

pub use app_data::*;

static POLL_TIMER_INTERVAL: Duration = Duration::from_millis(16);

fn main() -> Result<(), Box<dyn Error>> {
  ui::run_ui()
}
