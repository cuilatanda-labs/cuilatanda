use serde::{Serialize};

#[derive(Clone, Debug, Serialize)]
pub enum BarLineType {
  Single,
  Double,
  End,
  RepeatBegin,
  RepeatEnd,
  RepeatBoth,
  NONE,
}

#[derive(Clone, Debug, Serialize)]
pub struct BarLine {
  pub line_type: BarLineType
}

