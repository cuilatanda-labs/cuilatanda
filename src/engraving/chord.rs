use serde::{Serialize};
use crate::engraving::{Note, DurationType};

#[derive(Clone, Debug, Serialize)]
pub struct Chord {
  pub duration: DurationType,
  pub notes: Vec<Note>
}

impl Chord {
  pub fn create_default() -> Chord {
    let duration = DurationType::VWHOLE;
    let notes = vec![];
    Chord{
      duration,
      notes
    }
  }
}
