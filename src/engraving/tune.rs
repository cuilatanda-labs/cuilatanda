use serde::{Serialize};

#[derive(Clone, Debug, Serialize)]
pub struct Tune {
  pub id: char,
  pub key: char
}

impl Tune {
  pub fn create_default() -> Tune{
    Tune{
      id: '1',
      key: 'D'
    }
  }
}
