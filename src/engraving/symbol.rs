use serde::{Serialize};
use crate::engraving::{Chord, Rest, BarLine};

#[derive(Clone, Debug, Serialize)]
pub enum Symbol{
  Chord(Chord),
  Rest(Rest),
  BarLine(BarLine)
}
