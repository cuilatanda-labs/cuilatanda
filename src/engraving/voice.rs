use serde::{Serialize};
use crate::engraving::{Chord, Symbol, Rest};
//
#[derive(Clone, Debug, Serialize)]
pub struct Voice {
  pub symbols: Vec<Symbol>
}

impl Voice {
  pub fn create_default() -> Voice{
    let rest = Rest::create_default();
    let symbol = Symbol::Rest(rest);
    let symbols = vec![symbol];
    Voice{
      symbols
    }
  }
}
