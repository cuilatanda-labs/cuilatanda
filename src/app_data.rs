use vizia::prelude::*;

pub enum AppEvent {
  NewItem(String),
  AddItem,
  DeleteItem(usize),
  ToggleDone(usize),
}
