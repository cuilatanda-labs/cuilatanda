use vizia::{
  prelude::*,
  vg::{Align, Baseline, Paint, Path},
};

use crate::engraving::Note;

impl View for Note {
  fn draw(&self, cx: &mut DrawContext, canvas: &mut Canvas) {

  }
}

impl Note {
  pub fn pitch_number(&self) -> char {

    
    '1'
  }
}
