use std::path::PathBuf;

#[derive(Debug, Clone, PartialEq)]
pub enum UiEvent {
  // ----- Score Editor Input -----
  SymbolInput,
  DeleteSelectedLanes,
  ZoomIn,
  ZoomOut

}
