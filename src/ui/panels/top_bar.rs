use vizia::prelude::*;

pub fn top_bar(cx: &mut Context) {
  HStack::new(cx, |cx| {
    Label::new(cx, "top bar #1");
  })
    .class("top_bar");
}
