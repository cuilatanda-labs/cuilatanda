use vizia::prelude::*;

use crate::ui::Panel;

mod numbered_editor;
mod staff_editor;

pub fn score(cx: &mut Context) {
  VStack::new(cx, |cx| {
    Panel::new(
      cx,
      |cx| {
        Label::new(cx, "乐谱").class("small");
      },
      |cx| {
        ScrollView::new(cx, 0.0, 0.0, true, true, |cx| {
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
          Label::new(cx, "TIMELINE").class("small");
        }).class("score_content");
      },
    );
  })
    .row_between(Pixels(1.0))
    .class("score");
}
