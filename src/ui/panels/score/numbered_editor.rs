use vizia::{
  prelude::*,
  vg::{Align, Baseline, Paint, Path},
};
use vizia::text::{idx_to_pos, measure_text_lines, text_layout, text_paint_draw};
use vizia::vg::{RenderTarget, PixelFormat, ImageFlags, TextMetrics};
use vizia::resource::ImageOrId;
use std::ops::Range;
use std::option::Option::Some;
use std::prelude::rust_2015::Result::Ok;

pub struct ScoreNumberedEditor;

// Number Score Editor...
// Sync piano data in this state and render it.
impl View for ScoreNumberedEditor {
  fn draw(&self, cx: &mut DrawContext, canvas: &mut Canvas) {
    let bounds = cx.bounds();

  }
}
