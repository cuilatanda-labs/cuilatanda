use vizia::prelude::*;

use crate::ui::Panel;

pub fn left_bar(cx: &mut Context) {
  HStack::new(cx, |cx| {
    Panel::new(
      cx,
      |cx| {
        Label::new(cx, "符号面板").class("small");
      },
      |_cx| {

      },
    );
  })
    .class("left_bar");
}
