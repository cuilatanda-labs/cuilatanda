use vizia::{
  prelude::*,
  vg::{Align, Baseline, Paint, Path},
};
use vizia::text::{idx_to_pos, measure_text_lines, text_layout, text_paint_draw};
use vizia::vg::{RenderTarget, PixelFormat, ImageFlags, TextMetrics};
use vizia::resource::ImageOrId;
use std::ops::Range;
use std::option::Option::Some;
use std::prelude::rust_2015::Result::Ok;

use crate::ui::UiData;

pub struct Key {
  pub pitch: i32,
  pub k_type: i32,
  pub width: f32,
  pub height: f32
}

impl Key {
  pub fn draw(self, x:f32, y:f32, canvas: &mut Canvas) {
    let mut path = Path::new();
    path.move_to(x, y);
    path.line_to(x + self.width, y);
    path.line_to(x + self.width, y + self.height);
    path.line_to(x, y + self.height);
    canvas.stroke_path(&mut path, Paint::color(vizia::vg::Color::rgb(60, 63, 64)));
    canvas.fill_path(&mut path, Paint::color(vizia::vg::Color::rgb(255, 255, 255)));
  }
}


pub struct Keyboard;


impl Keyboard {
  pub fn new(cx: &mut Context) -> Handle<Self> {
    Self {}.build(cx, |_| {}).focusable(false).hoverable(false)
  }
}

impl View for Keyboard {
  fn draw(&self, cx: &mut DrawContext, canvas: &mut Canvas) {
    let bounds = cx.bounds();
    let key_width = 100.0;
    let key_height = key_width / 5.5;
    //get scale value in UiData's piano scale field..
    let scale = if let Some(ui_data) = cx.data::<UiData>() {
      ui_data.state.pianos.scale
    } else { 0 };

    println!("This piano roll view scale is {}", scale);
    canvas.save();
    canvas.scissor(bounds.x, bounds.y, bounds.w, bounds.h);
    for index in 1..10 {
      let key = Key{
        pitch: 60 + index,
        k_type: 0,
        width: key_width,
        height: key_height
      };
      key.draw(bounds.x, bounds.y + key_height * (1 + index) as f32, canvas);
    }

    canvas.restore();
  }
}
