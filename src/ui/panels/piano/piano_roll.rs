use vizia::prelude::*;
use crate::ui::{Panel, Keyboard};

pub fn piano_roll(cx: &mut Context) {
  VStack::new(cx, |cx| {
    Panel::new(
      cx,
      |cx| {
        Label::new(cx, Localized::new("piano-roll")).class("small");
      },
      |cx| {
        HStack::new(cx, |cx| {
          Keyboard::new(cx).class("key_board");
        }).row_between(Pixels(1.0));
      },
    )
      .class("piano_roll");
  })
    .row_between(Pixels(1.0))
    .class("piano_roll");
}
