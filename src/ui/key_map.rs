use vizia::prelude::*;
use crate::ui::UiEvent;

pub fn key_map(cx: &mut Context) {
  Keymap::from(vec![
    (
      KeyChord::new(Modifiers::empty(), Code::Digit1),
      KeymapEntry::new(UiEvent::DeleteSelectedLanes, |cx| {
        println!("key 1 is input.");
        cx.emit(UiEvent::DeleteSelectedLanes);
        cx.focus();
      }),
    ),
    (
      KeyChord::new(Modifiers::empty(), Code::Digit2),
      KeymapEntry::new(UiEvent::SymbolInput, |cx| {
        println!("key 2 is input.");
        cx.emit(UiEvent::SymbolInput);
        cx.focus();
      }),
    ),
    (
      KeyChord::new(Modifiers::CTRL, Code::Digit0),
      KeymapEntry::new(UiEvent::ZoomOut, |cx| {
        println!("key ZoomOut is input.");
        // Not work. keymap isn't binding of mac system.
        cx.emit(UiEvent::ZoomOut);
        cx.focus();
      }),
    ),
  ]).build(cx);
}
