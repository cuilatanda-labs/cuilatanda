use std::{error::Error, time::Duration};
use std::sync::{
  Arc,
  atomic::{AtomicBool, Ordering},
};

use unic_langid::langid;
use vizia::prelude::*;

pub use panels::*;
pub use views::*;

pub mod views;
pub mod panels;
pub mod key_map;
mod state;
pub use state::*;

use key_map::key_map;

const ZH_CN: LanguageIdentifier = langid!("zh");
static POLL_TIMER_INTERVAL: Duration = Duration::from_millis(16);

const MEADOWLARK_FONT: &[u8] = include_bytes!("./resources/fonts/Meadowlark.ttf");
const MIN_SANS_MEDIUM: &[u8] = include_bytes!("./resources/fonts/MinSans-Medium.otf");

pub fn run_ui() -> Result<(), Box<dyn Error>> {

  let run_poll_timer = Arc::new(AtomicBool::new(true));
  let run_poll_timer_clone = Arc::clone(&run_poll_timer);

  let app = Application::new(move |cx| {

    cx.emit(EnvironmentEvent::SetLocale(ZH_CN));
    cx.add_translation(
      ZH_CN,
      include_str!("./resources/zh/i18n.ftl").to_owned(),
    );
    cx.add_font_mem("meadowlark", MEADOWLARK_FONT);
    cx.add_font_mem("min-sans-medium", MIN_SANS_MEDIUM);

    cx.add_stylesheet("src/ui/resources/themes/default_theme/default_theme.css").expect("Failed to find default stylesheet");
    cx.add_stylesheet("src/ui/resources/themes/default_theme/top_bar.css").expect("Failed to find default stylesheet");
    cx.add_stylesheet("src/ui/resources/themes/default_theme/bottom_bar.css").expect("Failed to find default stylesheet");
    cx.add_stylesheet("src/ui/resources/themes/default_theme/score.css").expect("Failed to find default stylesheet");

    key_map(cx);

    UiData::new().unwrap().build(cx);

    VStack::new(cx, |cx| {
      top_bar(cx);
      HStack::new(cx, |cx| {
        left_bar(cx);
        VStack::new(cx, |cx| {
          score(cx);
          piano_roll(cx);
        })
          .overflow(Overflow::Hidden)
          .class("main");
      }).col_between(Pixels(1.0));
      bottom_bar(cx);
    })
      .background_color(Color::from("#0A0A0A"))
      .row_between(Pixels(1.0));

    let run_poll_timer_clone = Arc::clone(&run_poll_timer_clone);
    cx.spawn(move |_cx| {
      while run_poll_timer_clone.load(Ordering::Relaxed) {
        // cx.emit(UiEvent::PollEngine).unwrap();
        std::thread::sleep(POLL_TIMER_INTERVAL);
      }
    });

  })
    .title("吹拉弹打")
    // .canvas("canvas")
    .inner_size((1280, 720));

  app.run();
  run_poll_timer.store(false, Ordering::Relaxed);

  Ok(())
}
