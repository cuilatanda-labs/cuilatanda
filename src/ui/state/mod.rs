mod panel;
mod piano;

pub use panel::*;
pub use piano::*;

use std::error::Error;
use vizia::prelude::*;

#[derive(Lens)]
pub struct UiData {
  pub state: UiState,
}

impl UiData {
  pub fn new() -> Result<Self, Box<dyn Error>> {
    let panels = PanelState {
      hide_piano_roll: false,
    };
    let pianos = PianoRollState{ scale: 0 };
    let mut app_data = UiData {
      state: UiState { panels, pianos },
    };
    Ok(app_data)
  }
}

impl Model for UiData {
  fn event(&mut self, cx: &mut EventContext, event: &mut Event) {

  }
}


#[derive(Debug, Lens, Clone)]
pub struct UiState {
  pub panels: PanelState,
  pub pianos: PianoRollState,
}


impl UiState {

}
