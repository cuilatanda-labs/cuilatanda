use vizia::prelude::*;
use crate::ui::UiEvent;

#[derive(Debug, Lens, Clone)]
pub struct PianoRollState {
  // scale value in piano roll. default is 0
  pub scale: i32,
}

impl Model for PianoRollState {
  fn event(&mut self, cx: &mut EventContext, event: &mut Event) {
    event.map(|event, _| match event {
      UiEvent::ZoomIn => {
        self.scale -= 1;
      },
      UiEvent::ZoomOut => {
        self.scale += 1;
      },
      _ => {}
    });
  }
}
