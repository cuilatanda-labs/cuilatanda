use vizia::prelude::*;


#[derive(Debug, Lens, Clone)]
pub struct PanelState {
  pub hide_piano_roll: bool,
}


pub enum PanelEvent {
  TogglePianoRoll
}

impl Model for PanelState {
  fn event(&mut self, _: &mut EventContext, event: &mut Event) {}
}
